# Collection and transfer of Benthic Bugs

1. Five replicate samples are collected from the stream using a PVC core (0.008 m2) or surber sampler (0.092 m2).

2. Open whirlpack and rinse insides of bag with a little water.

3. Cut off top of whirlpack with scissors- avoid cutting off the sample date and replicate number.

4. Place 250 micron mesh ring on edge of sink and pour sample through the mesh- rinse inside of whirlrpack with distilled water to remove entire sample.

5. Rinse sample (now on mesh) with some water to wash off the 95% ETOH – at the same time begin moving the sample to one side of the mesh ring.

6. Using a flat metal spatula, transfer the sample to from the mesh ring to an 8 dram glass vial. Use a funnel and 70% ETOH to remove smaller pieces of sample.

7. Make sure the sample is covered with 70% ETOH in the 8 dram vial.

8. Label vial with baseline date, sample number, and PI initials

9. Rinse mesh ring with distilled water before transferring the next sample.

# Sample processing and calculations

1. Record on data sheet collection data from sample container (site, location, date, sample method)

2. Remove entire sample from jar or whirlpack and place in appropriate container (enamel pan, pitcher, or small bucgket).

3. Remove large debris such as sticks and rocks

4. Elutriate sample through a fine-mesh black sieve or plexiglass chamber fitted with fine mesh nitex; repeat 10x

5. If sample is to be “picked by eye”, place it in an enamel pan.

6. If sample is to be picked under a dissecting scope, place it in a grid petri dish.

7. Remove invertebrates; identify to designated taxonomic level; enumerate; place like individuals in properly labeled (site, location, date, contents) vial with 70% ETOH.

8. Do calculations (core is 0.008m2, surber is 0.092m2).

Note: Do not allow sample to dry out before or during the time that you are picking it.
Note: Some groups are undersampled with this method, i.e. the mobile groups like hemipterans and dystiscid beeteles.

