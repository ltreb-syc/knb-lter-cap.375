# knb-lter-cap.375

data-publishing resource for knb-lter-cap.375

*title*

Macroinvertebrate collections following floods in Sycamore Creek, Arizona, USA (1985-1999)

*abstract*

The primary objective of this project is to understand how long-term climate variability influences the structure and function of desert streams. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Arid regions are characterized by high interannual variation in precipitation, and these climate patterns drive the overall disturbance regime (in terms of flooding and drying) and nutrient status of desert stream ecosystems. At long time scales, the number and size of floods in a given year or cluster of years dictate nutrient delivery to streams from the surrounding catchment, and also influence the biogeomorphic structure of the stream-riparian corridor. Embedded within this decadal-scale hydrologic regime, flash floods scour stream channels and initiate a series of rapid successional changes by benthic algae and macroinvertebrates at short time scales (i.e., within a year). An important goal of this research is to determine how recovery following discrete events is influenced by both stream nutrient status and channel structure and to thus better understand how long-term climate variability and change guide the interactions among slow (biogeomorphic change) and fast (post-flood succession) features and processes characteristic of desert stream ecosystems.
